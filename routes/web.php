<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/notificacoes', 'NotificacaosController@index')->name('notificacoes.list');
    Route::get('/notificacao/{id}', 'NotificacaosController@show')->name('notificacao');

    Route::get('/map', 'MapController@index')->name('map');

    Route::get('/user', 'UserController@list')->name('user');
    // Route::get('/user/list/{id}', 'UserController@list_role')->name('user.list_role');
    Route::get('/user/add', 'UserController@add')->name('user.add');
    Route::post('/user/add', 'UserController@save')->name('user.save');
});
