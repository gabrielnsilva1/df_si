<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notificacao;

class NotificacaosController extends Controller
{
    public function index(){
       $notificacoes = Notificacao::all();
       return view('notificacao.list', compact('notificacoes'));
    }
    
    public function show($id){
       $notificacao = Notificacao::find($id);
       return view('notificacao.show', compact('notificacao'));
    }
}
