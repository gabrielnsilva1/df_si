<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserRole;

class UserController extends Controller
{
    public function list(){
       $users = User::all();
       return view('user.list', compact('users'));
    }

    public function list_role($role_id){
       $users = User::find()->where('user_role_id', $role_id);
       return view('user.list', compact('users'));
    }

    public function add(){
       $roles = UserRole::all();
       return view('user.add', compact('roles'));
    }

    public function save(Request $request){
        // dd($request->all());
        User::create([
            'name' => $request->name,
            'user_role_id' => (int)$request->user_role_id,
            'email' => $request->email,
            'active' => false,
        ]);
        return redirect()->route('user');
    }
}
