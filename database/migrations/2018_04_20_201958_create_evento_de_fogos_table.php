<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoDeFogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_de_fogos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('local_id')->unsigned()->nullable();
            $table->foreign('local_id')->references('id')->on('locals');
            $table->integer('terreno_id')->unsigned()->nullable();
            $table->foreign('terreno_id')->references('id')->on('terrenos');
            $table->integer('incendio_id')->unsigned()->nullable();
            $table->foreign('incendio_id')->references('id')->on('incendios');
            $table->integer('fumaca_id')->unsigned()->nullable();
            $table->foreign('fumaca_id')->references('id')->on('fumacas');
            $table->integer('clima_id')->unsigned()->nullable();
            $table->foreign('clima_id')->references('id')->on('climas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_de_fogos');
    }
}
