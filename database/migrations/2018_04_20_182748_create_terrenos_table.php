<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerrenosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terrenos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('topologia_id')->unsigned()->nullable();
            $table->foreign('topologia_id')->references('id')->on('topologias');
            $table->integer('vegetacao_id')->unsigned()->nullable();
            $table->foreign('vegetacao_id')->references('id')->on('vegetacaos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terrenos');
    }
}
