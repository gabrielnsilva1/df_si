<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadeDeConservacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidade_de_conservacaos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sigla')->nullable();
            // $table->geometryCollection('positions') // Possivel tipo de dado para tracar o perimetro da UC
            $table->integer('area')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidade_de_conservacaos');
    }
}
