<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncendiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incendios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_incendio_id')->unsigned()->default(2);
            $table->foreign('tipo_incendio_id')->references('id')->on('tipo_incendios');
            $table->double('intensidade', 5,2)->nullable();
            $table->string('dimensao')->nullable();
            $table->string('area_queimada')->nullable();
            $table->string('altura_chamas')->nullable();
            $table->string('direcao_alastramento')->nullable();
            $table->string('taxa_alastramento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incendios');
    }
}
