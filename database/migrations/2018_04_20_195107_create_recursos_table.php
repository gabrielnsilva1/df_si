<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_recurso_id')->unsigned();
            $table->foreign('tipo_recurso_id')->references('id')->on('tipo_recursos');
            $table->integer('estado_recurso_id')->unsigned();
            $table->foreign('estado_recurso_id')->references('id')->on('estado_recursos');
            $table->boolean('alocado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recursos');
    }
}
