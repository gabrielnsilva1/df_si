<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVitimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitimas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('atendida');
            $table->string('sexo');
            $table->integer('idade');
            $table->string('caracteristica'); // Para diferenciar vitmas
            $table->string('procedimentos_realizados')->nullable();
            $table->string('descricao_do_estado')->nullable();
            $table->string('escala_glasgow')->nullable();
            $table->string('classificacao_START')->nullable();
            $table->double('tempo_ao_hospital')->nullable();
            $table->boolean('necessita_helicoptero')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitimas');
    }
}
