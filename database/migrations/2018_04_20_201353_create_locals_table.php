<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('via')->nullable();
            $table->string('posicao')->nullable();
            $table->string('edificacoes_prox')->nullable();
            $table->string('via_acesso')->nullable();
            $table->double('lat', 9,6);
            $table->double('lon', 9,6);
            $table->double('alt', 9,6)->nullable();
            $table->boolean('dentro_uc')->nullable();
            $table->boolean('proximo_uc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locals');
    }
}
