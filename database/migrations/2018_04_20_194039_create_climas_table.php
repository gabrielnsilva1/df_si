<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climas', function (Blueprint $table) {
            $table->increments('id');
            $table->double('temperatura', 4,2)->nullable();
            $table->double('umidade', 4,2)->nullable();
            $table->double('velocidade_vento', 4,2)->nullable();
            $table->string('direcao_vento')->nullable();
            $table->integer('estacao_do_ano_id')->unsigned()->nullable();
            $table->foreign('estacao_do_ano_id')->references('id')->on('estacao_do_anos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climas');
    }
}
