<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Leonardo Botega',
            'user_role_id' => 1,
            'email' => 'Leonardo.Botega@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Allan Caixeta',
            'user_role_id' => 1,
            'email' => 'Allan.Caixeta@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Valdir Amancio',
            'user_role_id' => 2,
            'email' => 'Valdir.Amancio@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Jordan Ferreira',
            'user_role_id' => 2,
            'email' => 'Jordan.Ferreira@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Jordana Nogueira',
            'user_role_id' => 3,
            'email' => 'Jordana.Nogueira@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Gustavo Marttos',
            'user_role_id' => 3,
            'email' => 'Gustavo.Marttos@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Gabriel Nascimento',
            'user_role_id' => 4,
            'email' => 'Gabriel.Nascimento@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Cassio Santos',
            'user_role_id' => 4,
            'email' => 'Cassio.Santos@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Vinicius Mapelli',
            'user_role_id' => 5,
            'email' => 'Vinicius.Mapelli@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'Leonardo Sato',
            'user_role_id' => 5,
            'email' => 'Leonardo.Sato@brigadistas.org',
            'password' => bcrypt('123'),
            'active' => true,
        ]);
    }
}
