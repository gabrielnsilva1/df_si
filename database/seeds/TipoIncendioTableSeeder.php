<?php

use Illuminate\Database\Seeder;
use App\Models\TipoIncendio;

class TipoIncendioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoIncendio::create(['id' => 1, 'name' => 'Subterraneo', 'description' => 'Abaixo da superfície',]);
        TipoIncendio::create(['id' => 2, 'name' => 'Aereo', 'description' => 'Acima de 1,80m',]);
        TipoIncendio::create(['id' => 3, 'name' => 'Superficie', 'description' => 'Da superfície à 1,80m',]);
    }
}
