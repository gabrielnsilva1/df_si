<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::create(['id' => 1, 'role' => 'Bombeiro Chefe', 'description' => 'Bombeiro responsavel por todos os outros']);
        UserRole::create(['id' => 2, 'role' => 'Bombeiro', 'description' => 'Bombeiro responsavel por gerenciar gestores de UC']);
        UserRole::create(['id' => 3, 'role' => 'Gestor de UC', 'description' => 'Responsavel por gerenciar brigadistas de sua UC']);
        UserRole::create(['id' => 4, 'role' => 'Brigadista da UC', 'description' => 'Responsavel pelo combate inicial do incendio em uma UC']);
        UserRole::create(['id' => 5, 'role' => 'Civil', 'description' => 'Popular responsavel por comunicar focos de incendio']);
    }
}
