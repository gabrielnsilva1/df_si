<?php

use Illuminate\Database\Seeder;
use App\Models\Notificacao;

class NotificacaosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notificacao::create([
            'user_id' => 8,
            'text' => 'Fogoooo',
            'lat' => 14.895736,
            'lon' => -11.895736,
        ]);
        Notificacao::create([
            'user_id' => 9,
            'text' => 'Incendioooooo',
            'lat' => 14.895736,
            'lon' => -11.895736,
        ]);
        Notificacao::create([
            'user_id' => 10,
            'text' => 'Queimadaaaa',
            'lat' => 14.895736,
            'lon' => -11.895736,
        ]);
    }
}
