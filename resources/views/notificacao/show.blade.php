@extends('adminlte::page')

@section('title', 'Listagem de Notificações')

@section('content_header')
    <h1>Apresentação de Notificação</h1>
@stop

@section('content')
    <div class="box">
      <div class="box-header">
        <h3>Notificação #{{ $notificacao->id }}</h3>
      </div>
      <div class="box-body">
        <h4>Data</h4>
        <p>{{ $notificacao->created_at }}</p>
        <h4>User</h4>
        <p>{{ $notificacao->user_id }}</p>
        <h4>Texto</h4>
        <p>{{ $notificacao->text }}</p>
        <h4>Imagem</h4>
        @if($notificacao->image)
          <img src="" alt="">
        @else
          <p>Notificação sem imagem<i class="fa fa-fw fa-times-circle " style="color: red;"></i></p>
        @endif
        <h4>Audio</h4>
        @if($notificacao->audio)
          <audio src="">

          </audio>
        @else
          <p>Notificação sem audio<i class="fa fa-fw fa-times-circle " style="color: red;"></i></p>
        @endif
      </div>
    </div>
@stop
