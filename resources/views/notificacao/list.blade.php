@extends('adminlte::page')

@section('title', 'Listagem de Notificações')

@section('content_header')
    <h1>Listagem de Notificações</h1>
@stop

@section('content')
    <div class="box">
      <div class="box-header">

      </div>
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>User</th>
              <th>Texto</th>
              <th>Imagem</th>
              <th>Audio</th>
            </tr>
          </thead>
          <tbody>
            @forelse($notificacoes as $notificacao)
            <a href="{{ route('notificacao', [$notificacao->id]) }}">
              <tr>
                <td>{{ $notificacao->id }}</td>
                <td>{{ $notificacao->user_id }}</td>
                <td>{{ $notificacao->text }}</td>
                @if($notificacao->image)
                  <td><i class="fa fa-fw fa-check-circle " style="color: lime;"></i></td>
                @else
                  <td><i class="fa fa-fw fa-times-circle " style="color: red;"></i></td>
                @endif
                @if($notificacao->audio)
                  <td><i class="fa fa-fw fa-check-circle " style="color: lime;"></i></td>
                @else
                  <td><i class="fa fa-fw fa-times-circle " style="color: red;"></i></td>
                @endif
              </tr>
            </a>
            @empty
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
@stop
