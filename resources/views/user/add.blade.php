@extends('adminlte::page')

@section('title', 'Criação de Usuários')

@section('content_header')
    <h1>Criação de Usuário</h1>
@stop

@section('content')
    <form method="post" action="{{ route('user.save') }}">
      {!! csrf_field() !!}

      <div class="form-group">
        <label for="exampleFormControlInput1">Nome</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Nome Sobrenome">
      </div>
      <div class="form-group">
        <label for="exampleFormControlInput1">Email address</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect1">Papel</label>
        <select class="form-control col-12" id="user_role_id" name="user_role_id">
          <option value="0">Escolha...</option>
          @forelse($roles as $role)
          <option value="{{ $role->id }}">{{ $role->role }}</option>
          @empty
          @endforelse
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>
@stop
