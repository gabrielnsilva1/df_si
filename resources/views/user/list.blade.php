@extends('adminlte::page')

@section('title', 'Listagem de Usuários')

@section('content_header')
    <h1>Listagem de Usuário</h1>
@stop

@section('content')
    <div class="box">
      <div class="box-header">

      </div>
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            @forelse($users as $user)
            <tr>
              <td>{{ $user->id }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
            </tr>
            @empty
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
@stop
